import { TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { StoreModule } from '@ngrx/store';
import { NgxSpinnerModule } from 'ngx-spinner';

describe('AppComponent', () => {

  // let store: MockStore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule.forRoot(),
        ToastrModule.forRoot(),
        StoreModule.forRoot({}),
        NgxSpinnerModule
      ],
      declarations: [
        AppComponent,
        // provideMockStore({ initialState: null }),
      ],
    }).compileComponents();

    // store = TestBed.inject(MockStore);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should have a navbar with the text "Movie Search"', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const de = fixture.debugElement;
    expect(de.query(By.css('.navbar-brand')).nativeElement.innerText).toContain('Movie Search');
  });
});
