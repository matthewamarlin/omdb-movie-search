import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from './app-state';
import { OmdbPlotOptions } from './shared/models/omdb-plot-options.enum';
import { OmdbSingleSearchModel } from './shared/models/omdb-single-search.model';
import { OmdbSingleSearchResultModel } from './shared/models/omdb-single-search-result.model';
import { OmdbSearchType } from './shared/models/omdb-search-type.enum';
import { OmdbService } from './shared/services/omdb.service';
import { OmdbVideoType } from './shared/models/omdb-video-type';
import { OmdbSearchModel } from './shared/models/omdb-search.model.ts';
import { OmdbSearchResultModel } from './shared/models/omdb-search-result.model';
import { NgxMasonryComponent, NgxMasonryOptions } from 'ngx-masonry';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild(NgxMasonryComponent) masonry: NgxMasonryComponent;

  masonryOptions: NgxMasonryOptions = {
    gutter: 10,
    columnWidth: '.masonry-item',
    resize: true
  };

  form: FormGroup;
  searchForm: FormGroup;

  // Pagination
  _page = 1;
  collectionSize = 100; // TODO  
  pageSize = 5;

  searchResults: OmdbSearchResultModel;

  result: OmdbSingleSearchResultModel = {
    Title: 'Shrek',
    Year: '2001',
    Rated: 'PG',
    Released: '18 May 2001',
    Runtime: '90 min',
    Genre: 'Animation, Adventure, Comedy, Family, Fantasy',
    Director: 'Andrew Adamson, Vicky Jenson',
    Writer: 'William Steig (based upon the book by), Ted Elliott, Terry Rossio, Joe Stillman, Roger S.H. Schulman, Cody Cameron (additional dialogue), Chris Miller (additional dialogue), Conrad Vernon (additional dialogue)',
    Actors: 'Mike Myers, Eddie Murphy, Cameron Diaz, John Lithgow',
    Plot: 'A mean lord exiles fairytale creatures to the swamp of a grumpy ogre, who must go on a quest and rescue a princess for the lord in order to get his land back.',
    Language: 'English',
    Country: 'USA, Japan, France, Canada, Singapore, Portugal, UK',
    Awards: 'Won 1 Oscar. Another 39 wins & 60 nominations.',
    Poster: 'https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
    Ratings: [
      {
        Source: 'Internet Movie Database',
        Value: '7.8/10'
      },
      {
        Source: 'Rotten Tomatoes',
        Value: '88%'
      },
      {
        Source: 'Metacritic',
        Value: '84/100'
      }
    ],
    Metascore: '84',
    imdbRating: '7.8',
    imdbVotes: '614,721',
    imdbID: 'tt0126029',
    Type: 'movie',
    DVD: '25 Nov 2015',
    BoxOffice: '$267,665,011',
    Production: 'DreamWorks SKG, Pacific Data Images (PDI)',
    Website: 'N/A',
    Response: 'True'
  };

  // Make the enums available to the template
  OmdbVideoType = OmdbVideoType;
  OmdbPlotOptions = OmdbPlotOptions;
  OmdbSearchType = OmdbSearchType;

  constructor(
    private formBuilder: FormBuilder,
    private omdbService: OmdbService,
    private spinnerService: NgxSpinnerService,
    private store: Store<AppState>
  ) {
    this.initForm();
    this.initSearchForm();
  }

  initForm(): void {
    this.form = this.formBuilder.group({
      searchType: [OmdbSearchType.Title, Validators.required],
      term: ['', Validators.required],
      videoType: [OmdbVideoType.Any, null],
      year: [null, Validators.pattern('^[0-9]*$')],
      plot: [OmdbPlotOptions.Short, null]
    });
  }

  initSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      title: ['', Validators.required],
      videoType: [OmdbVideoType.Any, null],
      year: [null, Validators.pattern('^[0-9]*$')]
    });
  }

  singleSearch(): void {
    if (this.form.valid) {
      const model = this.mapFormToModel();

      console.log('model', model);

      // TODO Store ...
      this.omdbService.singleSearch(model).subscribe(result => {
        this.result = result;
      });
    }
  }

  search(page = 1): void {
    if (this.searchForm.valid) {
      this.spinnerService.show();
      const model = this.mapSearchFormToModel();
      model.page = page;

      console.log('model', model);

      // TODO Store ...
      this.omdbService.search(model).subscribe(result => {
        this.searchResults = result;
        this.collectionSize = +result.totalResults;
        this.pageSize = result.Search.length;
        this.spinnerService.hide();
      });
    }
  }

  mapFormToModel(): OmdbSingleSearchModel {
    // You can customise this function to return a different model shape,
    // but since ours is identicle to the form we just cast the form value.
    return this.form.value as OmdbSingleSearchModel;
  }

  mapSearchFormToModel(): OmdbSearchModel {
    return this.searchForm.value as OmdbSearchModel;
  }

  itemsLoaded(): void {
    console.log('items loaded');
  }

  get page(): number { return this._page; }
  set page(value: number) {
    this._page = value;
    this.search(this._page);
  }
}
