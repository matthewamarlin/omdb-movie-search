import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/app-state';
import { SearchState } from '../models/search-state.model';

export const getSearchState = createSelector(
    (state: AppState) => state.searchState,
    (searchState: SearchState) => searchState,
);
