import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/app-state';
import { AppConfigModel } from '../models/app-config.model';

export const getAppConfig = createSelector(
    (state: AppState) => state.appConfig,
    (appConfig: AppConfigModel) => appConfig,
);
