import { createReducer, on } from '@ngrx/store';
import { SearchState } from '../models/search-state.model';
import { clearSearchAction, searchSuccessAction } from '../actions/search-state.actions';

export const initialState: SearchState = null;

export const searchStateReducer = createReducer(
  initialState,
  on(clearSearchAction, _ => {
    return initialState;
  }),
  on(searchSuccessAction, (state: SearchState, { payload }) => {
    return Object.assign({}, state, { searchResults: payload.searchResult, search: payload.search });
  })
);
