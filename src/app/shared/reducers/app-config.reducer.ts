import { createReducer, on } from '@ngrx/store';
import { updateAppConfigAction } from '../actions/app-config.actions';
import { AppConfigModel } from '../models/app-config.model';

export const initialState: AppConfigModel = null;

export const appConfigReducer = createReducer(
  initialState,
  on(updateAppConfigAction, (state, { payload }) => Object.assign({}, state, payload))
);
