export interface OmdbSearchResultModel {
    Search: {
        Title: string;
        Year: string;
        imdbID: string;
        Type: string;
        Poster: string;
    }[];
    totalResults: string;
    Response: string;
    Error?: string;
}
