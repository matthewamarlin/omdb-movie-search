export interface AppConfigModel {
    version?: string;
    logging?: {
        levelMin?: string;
        levelMax?: string;
    };
    serviceUrl?: string;
    serviceApiKey?: string;
}
