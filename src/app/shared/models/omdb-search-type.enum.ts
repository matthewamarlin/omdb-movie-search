export enum OmdbSearchType {
    Id = 'By Id',
    Title = 'By Title',
}
