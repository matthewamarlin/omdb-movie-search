import { OmdbSearchResultModel } from './omdb-search-result.model';
import { OmdbSearchModel } from './omdb-search.model.ts';

export interface SearchState {
    search?: OmdbSearchModel;
    searchResults?: OmdbSearchResultModel;
}
