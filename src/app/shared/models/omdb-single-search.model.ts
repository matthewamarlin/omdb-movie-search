import { OmdbPlotOptions } from './omdb-plot-options.enum';
import { OmdbSearchType } from './omdb-search-type.enum';
import { OmdbVideoType } from './omdb-video-type';
export interface OmdbSingleSearchModel {
    term?: string;
    searchType?: OmdbSearchType;
    year?: number;
    videoType?: OmdbVideoType;
    plotOption?: OmdbPlotOptions;
}
