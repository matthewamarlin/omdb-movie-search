import { OmdbVideoType } from './omdb-video-type';

export interface OmdbSearchModel {
    title?: string;
    year?: string;
    videoType?: OmdbVideoType;
    page: number;
}
