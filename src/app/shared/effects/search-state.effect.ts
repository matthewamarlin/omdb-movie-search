import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';
import { SearchStateActionTypes } from '../actions/search-state.actions';
import { OmdbSearchResultModel } from '../models/omdb-search-result.model';
import { OmdbSearchModel } from '../models/omdb-search.model.ts';
import { OmdbService } from '../services/omdb.service';

@Injectable()
export class SearchEffects {

    search$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchStateActionTypes.SEARCH),
            mergeMap((action: any) => {
                return this.omdbService.search(action.payload)
                    .pipe(
                        map((result: OmdbSearchResultModel) => {

                            // This normally wouldnt be done itf the API returned appropriate Html errors.
                            if (result.Response === 'True') {
                                return {
                                    type: SearchStateActionTypes.SEARCH_SUCCESS,
                                    payload: {
                                        searchResult: result,
                                        search: action.payload
                                    }
                                };
                            }

                            return { type: SearchStateActionTypes.SEARCH_FAILED, payload: result.Error };
                        }),
                        catchError(() => of({ type: SearchStateActionTypes.SEARCH_FAILED }))
                    );
            })
        )
    );

    searchFailed$ = createEffect(() =>
        this.actions$.pipe(
            ofType(SearchStateActionTypes.SEARCH_FAILED),
            tap((action: any) => {
                this.toastr.error(action.payload, 'Search Failed');
            })
        ),
        { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private omdbService: OmdbService,
        private toastr: ToastrService
    ) { }
}
