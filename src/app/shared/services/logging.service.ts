import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app-state';
import { AppConfigModel } from '../models/app-config.model';
import { LogLevel } from '../models/log-levels.enum';

export abstract class Logger extends BaseService implements ILogger {

  protected levelMin: LogLevel;
  protected levelMax: LogLevel;

  constructor(store: Store<AppState>) {

    super(store);

    this.appConfig$.subscribe((config: AppConfigModel) => this.initConfig(config));
  }

  initConfig(config: AppConfigModel): void {
    this.levelMin = this.stringToLogLevel(config.logging.levelMin);
    this.levelMax = this.stringToLogLevel(config.logging.levelMax);
  }

  abstract write(level: LogLevel, message: string, exception?: string): void;
  abstract writeData(level: LogLevel, object: any, message?: string): void;

  private stringToLogLevel(value: string): LogLevel {
    if (value in LogLevel) {
      return LogLevel[value];
    } else {
      throw new Error(`Configuration Error: '${value}' is not a recognised LogLevel, the default value will be used.`);
    }
  }
}

@Injectable()
export class ConsoleLogger extends Logger {
  constructor(store: Store<AppState>) {
    super(store);
  }

  write(level: LogLevel, message: string, exception?: string): void {
    if (this.shouldLog(level)) {
      if (exception) {
        console.log(`${LogLevel[level]}: ${message}\r\n${exception}`);
      } else {
        console.log(`${LogLevel[level]}: ${message}`);
      }
    }
  }

  writeData(level: LogLevel, object: any, message?: string): void {
    if (this.shouldLog(level)) {
      if (message) {
        console.log(`${LogLevel[level]}: ${message}`);
      }
      console.log(object);
    }
  }

  private shouldLog = (level: LogLevel) => {
    return (level >= this.levelMin && level <= this.levelMax);
  }
}

export interface ILogger {
  write(level: LogLevel, message: string, exception?: string): void;
  writeData(level: LogLevel, object: any, message?: string): void;
}
