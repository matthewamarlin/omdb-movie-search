import { Store } from '@ngrx/store';
import { Observable, throwError } from 'rxjs';
import { AppState } from 'src/app/app-state';
import { AppConfigModel } from '../models/app-config.model';
import { getAppConfig } from '../selectors/app-config.selectors';

export abstract class BaseService {

    appConfig$: Observable<AppConfigModel>;
    appConfig: AppConfigModel;

    serviceUrl: string;

    constructor(store: Store<AppState>) {
        this.appConfig$ = store.select(getAppConfig);

        this.appConfig$.subscribe(config => {
            if (config) {
                this.initConfig(config);
            }
        });
    }

    initConfig(config: AppConfigModel): void {
        this.appConfig = config;

        this.serviceUrl = config.serviceUrl;
    }

    protected handleError(httpError: any): Observable<never> {

        console.log(httpError);

        const applicationError = httpError.headers ? httpError.headers.get('Application-Error') : null;

        // Either application-error in header or model error in body
        if (applicationError) {
            return throwError(applicationError);
        }

        return throwError(httpError.error || httpError || 'An unknown error occured.');
    }
}
