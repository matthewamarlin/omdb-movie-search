import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app-state';
import { OmdbSingleSearchModel } from '../models/omdb-single-search.model';
import { OmdbSingleSearchResultModel } from '../models/omdb-single-search-result.model';
import { OmdbSearchType } from '../models/omdb-search-type.enum';
import { OmdbPlotOptions } from '../models/omdb-plot-options.enum';
import { OmdbSearchModel } from '../models/omdb-search.model.ts';
import { OmdbVideoType } from '../models/omdb-video-type';
import { OmdbSearchResultModel } from '../models/omdb-search-result.model';

@Injectable()
export class OmdbService extends BaseService {

    constructor(
        store: Store<AppState>,
        private http: HttpClient
    ) {
        super(store);
    }

    singleSearch(model: OmdbSingleSearchModel): Observable<OmdbSingleSearchResultModel> {

        let parameters = new HttpParams()
            .set('apiKey', this.appConfig.serviceApiKey)
            .set('v', '1');


        switch (model.searchType) {
            case OmdbSearchType.Id:
                parameters = parameters.set('i', model.term);
                break;
            case OmdbSearchType.Title:
                parameters = parameters.set('t', model.term);
                break;
            default:
        }

        if (model.plotOption) {
            parameters = parameters.set('plot', model.plotOption ?? OmdbPlotOptions.Short);
        }

        if (model.videoType && model.videoType !== OmdbVideoType.Any) {
            parameters = parameters.set('type', model.videoType);
        }

        if (model.year) {
            parameters = parameters.set('y', model.year.toString());
        }

        return this.http.get<OmdbSingleSearchResultModel>(this.appConfig.serviceUrl, { params: parameters })
            .pipe(catchError(this.handleError));
    }

    search(model: OmdbSearchModel): Observable<OmdbSearchResultModel> {

        let parameters = new HttpParams()
            .set('apiKey', this.appConfig.serviceApiKey)
            .set('v', '1')
            .set('s', model.title ?? '');

        if (model.videoType && model.videoType !== OmdbVideoType.Any) {
            parameters = parameters.set('type', model.videoType);
        }

        if (model.year) {
            parameters = parameters.set('y', model.year.toString());
        }

        if (model.page) {
            parameters = parameters.set('page', model.page.toString());
        }

        return this.http.get<OmdbSearchResultModel>(this.appConfig.serviceUrl, { params: parameters })
            .pipe(catchError(this.handleError));
    }
}
