import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app-state';
import { AppConfigModel } from '../models/app-config.model';
import { updateAppConfigAction } from '../actions/app-config.actions';

@Injectable()
export class ConfigService {

  private config: AppConfigModel;

  onConfigChanged: BehaviorSubject<any>;

  constructor(
    private http: HttpClient,
    private store: Store<AppState>
  ) { }

  private setConfig(config: AppConfigModel): void {
    // Set the config locally
    this.config = config;

    // Create the behavior subject
    this.onConfigChanged = new BehaviorSubject(this.config);

    // Dispatch it to the NGRX store
    this.store.dispatch(updateAppConfigAction({ payload: config }));
  }

  load(url: string): Observable<any> {
    return this.http.get(url)
      .pipe(
        tap((jsonData: AppConfigModel) => this.setConfig(jsonData)),
        catchError((error) => {
          // TODO Extend Error Messages - More Detail
          throw new Error(`Error retrieving configuration from '${url}':\n ${error}`);
        })
      );
  }

  getConfig(): AppConfigModel {
    return this.config;
  }
}
