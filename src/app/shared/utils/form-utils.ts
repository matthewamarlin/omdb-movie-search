import { FormGroup, FormControl } from '@angular/forms';

export class FormUtils {
  static validateAllFormFields(formGroup: FormGroup): void {
    formGroup.markAsTouched({ onlySelf: true });
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  static findInvalidControls(formGroup: FormGroup): FormControl[] {
    const invalid = [];
    const controls = formGroup.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }
}
