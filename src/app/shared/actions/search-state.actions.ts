import { createAction, props } from '@ngrx/store';
import { OmdbSearchResultModel } from '../models/omdb-search-result.model';
import { OmdbSearchModel } from '../models/omdb-search.model.ts';

export enum SearchStateActionTypes {
  SEARCH = '[SEARCH STATE] Search',
  SEARCH_FAILED = '[SEARCH STATE] Search Failed',
  SEARCH_SUCCESS = '[SEARCH STATE] Search Success',
  CLEAR_SEARCH = '[SEARCH STATE] Clear Search'
}

export const searchAction = createAction(
  SearchStateActionTypes.SEARCH,
  props<{ payload: OmdbSearchModel }>()
);

export const searchSuccessAction = createAction(
  SearchStateActionTypes.SEARCH_SUCCESS,
  props<{
    payload: {
      searchResult: OmdbSearchResultModel,
      search: OmdbSearchModel
    }
  }>()
);

export const searchFailedAction = createAction(
  SearchStateActionTypes.SEARCH_FAILED
);

export const clearSearchAction = createAction(
  SearchStateActionTypes.CLEAR_SEARCH
);
