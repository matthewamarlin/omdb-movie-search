import { createAction, props } from '@ngrx/store';
import { AppConfigModel } from '../models/app-config.model';

export enum AppConfigActionTypes {
  LOAD = '[APPCONFIG] Load',
  UPDATE = '[APPCONFIG] Update',
}

export const loadAppConfigAction = createAction(
  AppConfigActionTypes.LOAD
);

export const updateAppConfigAction = createAction(
  AppConfigActionTypes.UPDATE,
  props<{ payload: AppConfigModel }>()
);
