import { AppConfigModel } from './shared/models/app-config.model';
import { SearchState } from './shared/models/search-state.model';

export interface AppState {
    readonly appConfig: AppConfigModel;
    readonly searchState: SearchState;
}
