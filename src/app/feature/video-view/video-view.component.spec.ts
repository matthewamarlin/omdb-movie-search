import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMasonryModule } from 'ngx-masonry';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VideoViewComponent } from './video-view.component';
import { OmdbService } from 'src/app/shared/services/omdb.service';
import { of } from 'rxjs';
import { StoreModule } from '@ngrx/store';
import { OmdbSingleSearchResultModel } from 'src/app/shared/models/omdb-single-search-result.model';

describe('VideoViewComponent', () => {

  const dummySearchResult = {
    Title: 'Indiana Jones and the Raiders of the Lost Ark',
    Year: '1981',
    Rated: 'PG',
    Released: '12 Jun 1981',
    Runtime: '115 min',
    Genre: 'Action, Adventure',
    Director: 'Steven Spielberg',
    Writer: 'Lawrence Kasdan (screenplay by), George Lucas (story by), Philip Kaufman (story by)',
    Actors: 'Harrison Ford, Karen Allen, Paul Freeman, Ronald Lacey',
    Plot: 'In 1936, archaeologist and adventurer Indiana Jones is hired by the U.S. government to find the Ark of the Covenant before Adolf Hitler\'s Nazis can obtain its awesome powers.',
    Language: 'English, German, Hebrew, Spanish, Arabic, Nepali',
    Country: 'USA, UK',
    Awards: 'Won 4 Oscars. Another 31 wins & 24 nominations.',
    Poster: 'https://m.media-amazon.com/images/M/MV5BMjA0ODEzMTc1Nl5BMl5BanBnXkFtZTcwODM2MjAxNA@@._V1_SX300.jpg',
    Ratings: [
      {
        Source: 'Internet Movie Database',
        Value: '8.4/10'
      },
      {
        Source: 'Rotten Tomatoes',
        Value: '95%'
      },
      {
        Source: 'Metacritic',
        Value: '85/100'
      }
    ],
    Metascore: '85',
    imdbRating: '8.4',
    imdbVotes: '885,825',
    imdbID: 'tt0082971',
    Type: 'movie',
    DVD: '28 Jan 2014',
    BoxOffice: '$248,159,971',
    Production: 'Paramount Pictures, Lucasfilm Ltd.',
    Website: 'N/A',
    Response: 'True'
  };

  const dummyErrorResponse: OmdbSingleSearchResultModel = {
    Response: 'False',
    Error: 'Incorrect IMDb ID.'
  };

  let fixture: ComponentFixture<VideoViewComponent>;
  let component: VideoViewComponent;
  let de: DebugElement;
  let omdbService: OmdbService;
  let singleSearchSpy: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        NgxSpinnerModule,
        RouterTestingModule,
        StoreModule.forRoot({}),
        SharedModule.forRoot(),
        ToastrModule.forRoot(),
        NgbModule,
        NgxMasonryModule
      ],
      declarations: [
        VideoViewComponent,
      ],
      providers: [
        OmdbService
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(VideoViewComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    omdbService = de.injector.get(OmdbService);
    singleSearchSpy = spyOn(omdbService, 'singleSearch').and.returnValue(of(dummySearchResult));
    // store = TestBed.inject(MockStore);

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call the OmdbService to try load the Movie', () => {
    expect(singleSearchSpy).toHaveBeenCalled();
    expect(component.result).toEqual(dummySearchResult);
  });

  it('should display the correct movie information when there is a valid response', () => {
    expect(de.query(By.css('.info-container'))).toBeTruthy();
    expect(de.query(By.css('#GenreValue')).nativeElement.innerText).toEqual(dummySearchResult.Genre);
    expect(de.query(By.css('#LanguageValue')).nativeElement.innerText).toEqual(dummySearchResult.Language);
    expect(de.query(By.css('#RatedValue')).nativeElement.innerText).toEqual(dummySearchResult.Rated);
    expect(de.query(By.css('#ActorsValue')).nativeElement.innerText).toEqual(dummySearchResult.Actors);
    expect(de.query(By.css('#PlotValue')).nativeElement.innerText).toEqual(dummySearchResult.Plot);
    expect(de.query(By.css('#ProductionValue')).nativeElement.innerText).toEqual(dummySearchResult.Production);
    expect(de.query(By.css('#DirectorValue')).nativeElement.innerText).toEqual(dummySearchResult.Director);
    expect(de.query(By.css('#ReleasedValue')).nativeElement.innerText).toEqual(dummySearchResult.Released);
    expect(de.query(By.css('#CountryValue')).nativeElement.innerText).toEqual(dummySearchResult.Country);
    expect(de.query(By.css('#BoxOfficeValue')).nativeElement.innerText).toEqual(dummySearchResult.BoxOffice);
    expect(de.query(By.css('#DVDValue')).nativeElement.innerText).toEqual(dummySearchResult.DVD);
    expect(de.query(By.css('#WebsiteValue')).nativeElement.innerText).toEqual(dummySearchResult.Website);
    expect(de.query(By.css('#imdbIDValue')).nativeElement.innerText).toEqual(dummySearchResult.imdbID);
    expect(de.query(By.css('#imdbVotesValue')).nativeElement.innerText).toEqual(dummySearchResult.imdbVotes);
  });

  it('should display an error message when there is an invalid response', () => {
    // (omdbService.singleSearch as jasmine.Spy).and.returnValue(of(dummyErrorResponse));
    singleSearchSpy.and.returnValue(of(dummyErrorResponse));
    component.ngOnInit();

    omdbService.singleSearch(null).subscribe(x => {
      console.log('man call', x);
    });

    fixture.detectChanges();

    expect(singleSearchSpy).toHaveBeenCalled();
    expect(component.result).toEqual(dummyErrorResponse);
    expect(de.query(By.css('.error-container'))).toBeTruthy();
    expect(de.query(By.css('.error-container .alert')).nativeElement.innerText).toEqual(dummyErrorResponse.Error);
  });
});
