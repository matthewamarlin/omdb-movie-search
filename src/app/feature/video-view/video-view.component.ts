import { Component, OnInit } from '@angular/core';
import { OmdbSingleSearchResultModel } from '../../shared/models/omdb-single-search-result.model';
import { OmdbSearchType } from '../../shared/models/omdb-search-type.enum';
import { OmdbService } from '../../shared/services/omdb.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.scss']
})
export class VideoViewComponent implements OnInit {

  imdbID: string;
  result: OmdbSingleSearchResultModel;
  error: string;

  constructor(
    private omdbService: OmdbService,
    private spinnerService: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(parameter => {
      this.search(parameter.id);
    });
  }

  search(imdbID: string): void {
    this.imdbID = imdbID;
    this.spinnerService.show();

    this.omdbService.singleSearch({ searchType: OmdbSearchType.Id, term: this.imdbID }).pipe(catchError(err => {
      return throwError(err);
    })).subscribe(result => {
      this.result = result;
      this.spinnerService.hide();
    });
  }
}
