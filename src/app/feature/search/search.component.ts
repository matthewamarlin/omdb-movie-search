import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app-state';
import { OmdbPlotOptions } from '../../shared/models/omdb-plot-options.enum';
import { OmdbSearchType } from '../../shared/models/omdb-search-type.enum';
import { OmdbService } from '../../shared/services/omdb.service';
import { OmdbVideoType } from '../../shared/models/omdb-video-type';
import { OmdbSearchModel } from '../../shared/models/omdb-search.model.ts';
import { OmdbSearchResultModel } from '../../shared/models/omdb-search-result.model';
import { NgxMasonryComponent, NgxMasonryOptions } from 'ngx-masonry';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormUtils } from 'src/app/shared/utils/form-utils';
import { ToastrService } from 'ngx-toastr';
import { SearchState } from 'src/app/shared/models/search-state.model';
import { SearchStateActionTypes } from 'src/app/shared/actions/search-state.actions';
import { getSearchState } from 'src/app/shared/selectors/search-state.selectors';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  // Masonry Settings
  @ViewChild(NgxMasonryComponent) masonry: NgxMasonryComponent;

  masonryOptions: NgxMasonryOptions = {
    gutter: 10,
    columnWidth: '.masonry-item',
    resize: true
  };

  // Search Related State And Form
  searchState: SearchState;
  searchForm: FormGroup;

  // Make the enums available to the template
  OmdbVideoType = OmdbVideoType;
  OmdbPlotOptions = OmdbPlotOptions;
  OmdbSearchType = OmdbSearchType;

  constructor(
    private formBuilder: FormBuilder,
    private omdbService: OmdbService,
    private spinnerService: NgxSpinnerService,
    private store: Store<AppState>,
    private toastr: ToastrService
  ) {
    this.initSearchForm();

    this.store.select(getSearchState).subscribe(state => {
      this.spinnerService.show();

      this.setFormData(state?.search);
      console.log('Setting state', state);
      this.searchState = state;

      this.spinnerService.hide();
    });
  }

  // Initialises the form with the appropriate validators and default values
  initSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      title: ['', Validators.required],
      videoType: [OmdbVideoType.Any, null],
      year: [null, [Validators.pattern(/^[0-9]*$/)]]
    });
  }

  // Sets the forms data given the search model and marks the form as untouched
  setFormData(search?: OmdbSearchModel): void {
    this.searchForm.patchValue({
      title: search?.title,
      videoType: search?.videoType ?? OmdbVideoType.Any,
      year: search?.year
    });
    this.searchForm.markAsUntouched();
  }

  // Performs a new search given using the form data
  search(): void {
    if (this.searchForm.valid) {
      const model = this.mapSearchFormToModel();
      model.page = 1;

      this.store.dispatch({ type: SearchStateActionTypes.SEARCH, payload: model });

      return;
    }

    FormUtils.validateAllFormFields(this.searchForm);
  }

  clear(): void {
    this.store.dispatch({ type: SearchStateActionTypes.CLEAR_SEARCH });
  }

  mapSearchFormToModel(): OmdbSearchModel {
    return this.searchForm.value as OmdbSearchModel;
  }

  pageChanged(pageNumber: number): void {
    const model = Object.assign({}, this.searchState.search);
    model.page = pageNumber;

    this.store.dispatch({ type: SearchStateActionTypes.SEARCH, payload: model });
  }
}
