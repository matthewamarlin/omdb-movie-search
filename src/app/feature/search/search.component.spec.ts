import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule, SpyNgModuleFactoryLoader } from '@angular/router/testing';
import { State, Store, StoreModule } from '@ngrx/store';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/app/app-state';
import { SharedModule } from 'src/app/shared/shared.module';
import { SearchComponent } from './search.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { SearchState } from 'src/app/shared/models/search-state.model';
import { getSearchState } from 'src/app/shared/selectors/search-state.selectors';
import { OmdbVideoType } from 'src/app/shared/models/omdb-video-type';
import { NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMasonryModule } from 'ngx-masonry';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('SearchComponent', () => {

  const initialState: AppState = {
    appConfig: {
      logging: {
        levelMax: 'Trace'
      }
    },
    searchState: null
  };

  let fixture: ComponentFixture<SearchComponent>;
  let component: SearchComponent;
  let de: DebugElement;
  let store: MockStore;

  const randomState: SearchState = {
    search: {
      page: 1
    },
    searchResults: null
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        NgxSpinnerModule,
        RouterTestingModule,
        SharedModule.forRoot(),
        ToastrModule.forRoot(),
        NgbModule,
        NgxMasonryModule
      ],
      declarations: [
        SearchComponent,
      ],
      providers: [
        provideMockStore({
          initialState: null,
          selectors: [
            { selector: getSearchState, value: null },
          ]
        }),
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    store = TestBed.inject(MockStore);

    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a search control', () => {
    expect(de.query(By.css('.search-controls-container'))).toBeTruthy();
  });

  it('should have the same state locally as it does in the store', () => {
    store.overrideSelector(getSearchState, randomState);
    store.refreshState();
    console.log(component.searchState);
    expect(component.searchState).toEqual(randomState);
  });

  it('should display an information card when the search state is null', () => {
    expect(component.searchState).toEqual(null);
    expect(de.query(By.css('.search-instructions')).nativeElement).toBeTruthy();
  });

  it('should set the form text based on the previous search state', () => {
    const state: SearchState = {
      search: {
        page: 1,
        title: 'Matthew',
        videoType: OmdbVideoType.Any,
        year: '2001'
      }
    };

    store.overrideSelector(getSearchState, state);
    store.refreshState();

    fixture.detectChanges();

    expect(component.searchState).toEqual(state);
    expect(de.query(By.css('#InputTitle')).nativeElement.value).toEqual(state.search.title);
    expect(de.query(By.css('#InputYear')).nativeElement.value).toEqual(state.search.year);
    expect(de.query(By.css('#SelectVideoType')).nativeElement.value).toEqual(state.search.videoType);
  });

  it('should display the masonry grid and a pagination control when there are search results', () => {
    const state: SearchState = {
      search: {
        page: 1,
        title: 'Matthew',
        videoType: OmdbVideoType.Any,
        year: null
      },
      searchResults: {
        Response: 'True',
        Search: [
          {
            Title: 'Iron Man',
            Year: '2008',
            imdbID: 'tt0371746',
            Type: 'movie',
            Poster: 'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg'
          }
        ],
        totalResults: '1',
      }
    };

    store.overrideSelector(getSearchState, state);
    store.refreshState();

    fixture.detectChanges();

    expect(component.searchState).toEqual(state);
    expect(de.query(By.css('.search-result-container')).nativeElement).toBeTruthy();
    expect(de.query(By.css('.search-pagination-container')).nativeElement).toBeTruthy();
    expect(de.query(By.css(`.pagination li:nth-child(${state.search.page + 1})`)).nativeElement).toBeTruthy();
  });

  it('pagination control should set to the correct page based on the state', () => {
    const state: SearchState = {
      search: {
        page: 2,
        title: 'Matthew',
        videoType: OmdbVideoType.Any,
        year: null
      },
      searchResults: {
        Response: 'True',
        Search: [
          {
            Title: 'Iron Man',
            Year: '2008',
            imdbID: 'tt0371746',
            Type: 'movie',
            Poster: 'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_SX300.jpg'
          }
          // More left out for brevity
        ],
        totalResults: '20',
      }
    };

    store.overrideSelector(getSearchState, state);
    store.refreshState();

    fixture.detectChanges();

    expect(component.searchState).toEqual(state);
    expect(de.query(By.css(`.pagination li:nth-child(${state.search.page + 1})`)).nativeElement).toBeTruthy();
  });
});
