import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { appInitializer } from './app.startup';
import { ConfigService } from './shared/services/config.service';
import { ConsoleLogger, Logger } from './shared/services/logging.service';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { appConfigReducer } from './shared/reducers/app-config.reducer';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMasonryModule } from 'ngx-masonry';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SearchComponent } from './feature/search/search.component';
import { VideoViewComponent } from './feature/video-view/video-view.component';
import { ToastrModule } from 'ngx-toastr';
import { searchStateReducer } from './shared/reducers/search-state.reducer';
import { SearchEffects } from './shared/effects/search-state.effect';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    VideoViewComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,

    SharedModule.forRoot(),

    // NGRX - Store Configuration
    StoreModule.forRoot({
      appConfig: appConfigReducer,
      searchState: searchStateReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([
      SearchEffects
    ]),
    // Ng Bootstrap
    NgbModule,
    // Masonary
    NgxMasonryModule,
    // Spinner
    NgxSpinnerModule,
    // Toastr
    ToastrModule.forRoot()
  ],
  providers: [
    ConfigService,
    // Logger
    ConsoleLogger,
    {
      provide: Logger,
      useExisting: ConsoleLogger
    },
    // App Initializer
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      deps: [
        ConfigService
      ],
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
