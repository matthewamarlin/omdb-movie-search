/* This class is the startup logic & helper functions for configuring globally available modules */

import { environment } from '../environments/environment';
import { ConfigService } from './shared/services/config.service';

/**
 * Runs on application startup to initialise services.
 */
export function appInitializer(configService: ConfigService): () => Promise<any> {
    return () => new Promise((resolve) => {

        if (environment.production) {
            console.log('Production mode enabled!');
        } else {
            console.log('Development mode enabled!');
        }

        configService.load(environment.configFile).subscribe(() => {
            resolve(true);
        });
    });
}
