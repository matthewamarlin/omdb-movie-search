# OMDB (Open Movie Database) API Search

## About
                    
This project serves to illustrates how to Connect an Angular front end to a Restful API.
It uses the <a target="_blank" href="http://omdbapi.com/">OMDB (Open Movie Database)</a> API as a backend.
If you clone this repository, please ensure you update the application configuration file (found under /assets), with an <a target="_blank" href="http://omdbapi.com/apikey.aspx">API Key from OMDB.</a>


The <a target="_blank" href="http://omdbapi.com/#parameters">V1 OMDB API</a> essentially defines 3 ways to search for Movies:


- Get By ID - Which <b>finds the exact movie by its ID</b>. The result includes all the information about the movie.
- Get By Title - Which <b>finds the first movie to match the given title</b>. The result includes all the information about the movie.
- Get By Search - Which <b>searches by title and page</b> and returns 10 movies matching the search. The result only includes a limited subset of information about the movie

## Key Concepts

- Angular Services, Components and Routing
- Responsive Styling using Bootstrap
- State Management using Redux - Actions, Reducers & Effects
- Using Masonry for dynamic Grid tiling
- An example of how to Do Unit, Integration and End to End Testing (with code Coverage Reports)

## Technology / Frameworks
- Angular 11
- Testing with Karma, Jasmine & Protractor
- Bootstrap 4 - Bootstrap Styling
- NgBootstrap - Bootstrap Components for Angular
- NgRx - Redux for Angular
- NgxToastr - Toast Notifications

## Out of Scope
- Angular Modules & Lazy Loading

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

- [Visual Studio Code](https://code.visualstudio.com/)
- [Node.js 12.18.1](https://nodejs.org/dist/v12.18.1/)

### Getting the development environment running

Open the root repository directory with VS Code 2019 and run the following commands:
 
```
npm install
npm start
```

Now you will be able to navigate to [http://localhost:4200](http://localhost:4200)


## Authors

Created with ❤️ by Matthew Marlin - <a href="https://gitlab.com/users/matthewamarlin/projects">GitLab</a>
